<?php

class countryCodes
{

    private $countryData = ARRAY(
        array("name"  =>"AF","code" => 93),
        array("name"  =>"AX","code" => 35818),
        array("name"  =>"AL","code" => 355),
        array("name"  =>"DZ","code" => 213),
        array("name"  =>"AS","code" => 1684),
        array("name"  =>"AD","code" => 376),
        array("name"  =>"AO","code" => 244),
        array("name"  =>"AI","code" => 1264),
        array("name"  =>"AG","code" => 1268),
        array("name"  =>"AR","code" => 54),
        array("name"  =>"AM","code" => 374),
        array("name"  =>"AW","code" => 297),
        array("name"  =>"SH","code" => 247),
        array("name"  =>"AU","code" => 61),
        array("name"  =>"AU","code" => 6721),
        array("name"  =>"AT","code" => 672),
        array("name"  =>"AZ","code" => 43),
        array("name"  =>"AZ","code" => 994),
        array("name"  =>"BS","code" => 1242),
        array("name"  =>"BH","code" => 973),
        array("name"  =>"BD","code" => 880),
        array("name"  =>"BB","code" => 1246),
        array("name"  =>"AG","code" => 1268),
        array("name"  =>"BY","code" => 375),
        array("name"  =>"BE","code" => 32),
        array("name"  =>"BZ","code" => 501),
        array("name"  =>"BJ","code" => 229),
        array("name"  =>"BM","code" => 1441),
        array("name"  =>"BT","code" => 975),
        array("name"  =>"BO","code" => 591),
        array("name"  =>"BQ","code" => 5997),
        array("name"  =>"BA","code" => 387),
        array("name"  =>"BW","code" => 267),
        array("name"  =>"BR","code" => 55),
        array("name"  =>"IO","code" => 246),
        array("name"  =>"VG","code" => 1284),
        array("name"  =>"BN","code" => 673),
        array("name"  =>"BG","code" => 359),
        array("name"  =>"BF","code" => 226),
        array("name"  =>"BI","code" => 257),
        array("name"  =>"CV","code" => 238),
        array("name"  =>"KH","code" => 855),
        array("name"  =>"CM","code" => 237),
        array("name"  =>"CA","code" => 1),
        array("name"  =>"BQ","code" => 5993),
        array("name"  =>"BQ","code" => 5994),
        array("name"  =>"BQ","code" => 5997),
        array("name"  =>"KY","code" => 1345),
        array("name"  =>"CF","code" => 236),
        array("name"  =>"TD","code" => 235),
        array("name"  =>"NZ","code" => 64),
        array("name"  =>"CL","code" => 56),
        array("name"  =>"CN","code" => 86),
        array("name"  =>"CX","code" => 6189164),
        array("name"  =>"CC","code" => 6189162),
        array("name"  =>"CO","code" => 57),
        array("name"  =>"KM","code" => 269),
        array("name"  =>"CG","code" => 242),
        array("name"  =>"CD","code" => 243),
        array("name"  =>"CK","code" => 682),
        array("name"  =>"CR","code" => 506),
        array("name"  =>"HR","code" => 225),
        array("name"  =>"CU","code" => 385),
        array("name"  =>"CW","code" => 53),
        array("name"  =>"CY","code" => 5999),
        array("name"  =>"CY","code" => 357),
        array("name"  =>"CZ","code" => 420),
        array("name"  =>"DK","code" => 45),
        array("name"  =>"IO","code" => 246),
        array("name"  =>"DJ","code" => 253),
        array("name"  =>"DM","code" => 1767),
        array("name"  =>"DO","code" => 1809),
        array("name"  =>"DO","code" => 1829),
        array("name"  =>"DO","code" => 1849),
        array("name"  =>"CL","code" => 56),
        array("name"  =>"EC","code" => 593),
        array("name"  =>"EG","code" => 20),
        array("name"  =>"SV","code" => 503),
        array("name"  =>"","code" => 8812),
        array("name"  =>"","code" => 8813),
        array("name"  =>"","code" => 88213),
        array("name"  =>"GQ","code" => 240),
        array("name"  =>"ER","code" => 291),
        array("name"  =>"EE","code" => 372),
        array("name"  =>"SZ","code" => 268),
        array("name"  =>"ET","code" => 251),
        array("name"  =>"FK","code" => 500),
        array("name"  =>"FO","code" => 298),
        array("name"  =>"FJ","code" => 679),
        array("name"  =>"FI","code" => 358),
        array("name"  =>"FR","code" => 33),
        array("name"  =>"FR","code" => 596),
        array("name"  =>"GF","code" => 594),
        array("name"  =>"PF","code" => 689),
        array("name"  =>"GA","code" => 241),
        array("name"  =>"GM","code" => 220),
        array("name"  =>"GE","code" => 995),
        array("name"  =>"DE","code" => 49),
        array("name"  =>"GH","code" => 233),
        array("name"  =>"GI","code" => 350),
        array("name"  =>"","code" => 881),
        array("name"  =>"","code" => 8818),
        array("name"  =>"","code" => 8819),
        array("name"  =>"GR","code" => 30),
        array("name"  =>"GL","code" => 299),
        array("name"  =>"GD","code" => 1473),
        array("name"  =>"GP","code" => 590),
        array("name"  =>"GU","code" => 1671),
        array("name"  =>"GT","code" => 502),
        array("name"  =>"GG","code" => 441481),
        array("name"  =>"GG","code" => 447781),
        array("name"  =>"GG","code" => 447839),
        array("name"  =>"GG","code" => 447911),
        array("name"  =>"GN","code" => 224),
        array("name"  =>"GW","code" => 245),
        array("name"  =>"GY","code" => 592),
        array("name"  =>"HT","code" => 509),
        array("name"  =>"HN","code" => 504),
        array("name"  =>"HK","code" => 852),
        array("name"  =>"HU","code" => 36),
        array("name"  =>"IS","code" => 354),
        array("name"  =>"","code" => 8810),
        array("name"  =>"","code" => 8811),
        array("name"  =>"IN","code" => 91),
        array("name"  =>"ID","code" => 62),
        array("name"  =>"","code" => 870),
        array("name"  =>"","code" => 800),
        array("name"  =>"","code" => 882),
        array("name"  =>"","code" => 883),
        array("name"  =>"","code" => 979),
        array("name"  =>"","code" => 808),
        array("name"  =>"IR","code" => 98),
        array("name"  =>"IQ","code" => 964),
        array("name"  =>"IE","code" => 353),
        array("name"  =>"","code" => 8816),
        array("name"  =>"","code" => 8817),
        array("name"  =>"IM","code" => 447524,),
        array("name"  =>"IM","code" => 447524),
        array("name"  =>"IM","code" => 447624),
        array("name"  =>"IM","code" => 447924),
        array("name"  =>"IL","code" => 972),
        array("name"  =>"IT","code" => 39),
        array("name"  =>"JM","code" => 1658),
        array("name"  =>"JM","code" => 1876),
        array("name"  =>"SJ","code" => 4779),
        array("name"  =>"JP","code" => 81),
        array("name"  =>"JE","code" => 441534),
        array("name"  =>"JO","code" => 962),
        array("name"  =>"KZ","code" => 76),
        array("name"  =>"KZ","code" => 77),
        array("name"  =>"KE","code" => 254),
        array("name"  =>"KI","code" => 686),
        array("name"  =>"KP","code" => 850),
        array("name"  =>"KR","code" => 82),
        array("name"  =>"RS","code" => 383),
        array("name"  =>"KW","code" => 965),
        array("name"  =>"KG","code" => 996),
        array("name"  =>"LA","code" => 856),
        array("name"  =>"LV","code" => 371),
        array("name"  =>"LB","code" => 961),
        array("name"  =>"LS","code" => 266),
        array("name"  =>"LR","code" => 231),
        array("name"  =>"LY","code" => 218),
        array("name"  =>"LI","code" => 423),
        array("name"  =>"LT","code" => 370),
        array("name"  =>"LU","code" => 352),
        array("name"  =>"MO","code" => 853),
        array("name"  =>"MG","code" => 261),
        array("name"  =>"MW","code" => 265),
        array("name"  =>"MY","code" => 60),
        array("name"  =>"MV","code" => 960),
        array("name"  =>"ML","code" => 223),
        array("name"  =>"MT","code" => 356),
        array("name"  =>"MH","code" => 692),
        array("name"  =>"MQ","code" => 596),
        array("name"  =>"MR","code" => 222),
        array("name"  =>"MU","code" => 230),
        array("name"  =>"YT","code" => 262269),
        array("name"  =>"YT","code" => 262639),
        array("name"  =>"MX","code" => 52),
        array("name"  =>"FM","code" => 691),
        array("name"  =>"US","code" => 1808),
        array("name"  =>"MD","code" => 373),
        array("name"  =>"MC","code" => 377),
        array("name"  =>"MN","code" => 976),
        array("name"  =>"ME","code" => 382),
        array("name"  =>"MS","code" => 1664),
        array("name"  =>"MA","code" => 212),
        array("name"  =>"MZ","code" => 258),
        array("name"  =>"MM","code" => 95),
        array("name"  =>"AM","code" => 37447),
        array("name"  =>"AM","code" => 37497),
        array("name"  =>"NA","code" => 264),
        array("name"  =>"NR","code" => 674),
        array("name"  =>"NP","code" => 977),
        array("name"  =>"NL","code" => 31),
        array("name"  =>"KN","code" => 1869),
        array("name"  =>"NC","code" => 687),
        array("name"  =>"NZ","code" => 64),
        array("name"  =>"NI","code" => 505),
        array("name"  =>"NE","code" => 227),
        array("name"  =>"NG","code" => 234),
        array("name"  =>"NU","code" => 683),
        array("name"  =>"NF","code" => 6723),
        array("name"  =>"MK","code" => 389),
        array("name"  =>"CY","code" => 90392),
        array("name"  =>"GB","code" => 4428),
        array("name"  =>"MP","code" => 1670),
        array("name"  =>"NO","code" => 47),
        array("name"  =>"OM","code" => 968),
        array("name"  =>"PK","code" => 92),
        array("name"  =>"PW","code" => 680),
        array("name"  =>"PS","code" => 970),
        array("name"  =>"PA","code" => 507),
        array("name"  =>"PG","code" => 675),
        array("name"  =>"PY","code" => 595),
        array("name"  =>"PE","code" => 51),
        array("name"  =>"PH","code" => 63),
        array("name"  =>"PN","code" => 64),
        array("name"  =>"PL","code" => 48),
        array("name"  =>"PT","code" => 351),
        array("name"  =>"PR","code" => 1787),
        array("name"  =>"PR","code" => 1939),
        array("name"  =>"QA","code" => 974),
        array("name"  =>"RE","code" => 262),
        array("name"  =>"RO","code" => 40),
        array("name"  =>"RU","code" => 7),
        array("name"  =>"RW","code" => 250),
        array("name"  =>"BQ","code" => 5994),
        array("name"  =>"BL","code" => 590),
        array("name"  =>"SH","code" => 290),
        array("name"  =>"KN","code" => 1869),
        array("name"  =>"LC","code" => 1758),
        array("name"  =>"MF","code" => 590),
        array("name"  =>"PM","code" => 508),
        array("name"  =>"VC","code" => 1784),
        array("name"  =>"WS","code" => 685),
        array("name"  =>"SM","code" => 378),
        array("name"  =>"ST","code" => 239),
        array("name"  =>"SA","code" => 966),
        array("name"  =>"SN","code" => 221),
        array("name"  =>"RS","code" => 381),
        array("name"  =>"SC","code" => 248),
        array("name"  =>"SL","code" => 232),
        array("name"  =>"SG","code" => 65),
        array("name"  =>"BQ","code" => 5993),
        array("name"  =>"SX","code" => 1721),
        array("name"  =>"SK","code" => 421),
        array("name"  =>"SI","code" => 386),
        array("name"  =>"SB","code" => 677),
        array("name"  =>"SO","code" => 252),
        array("name"  =>"ZA","code" => 27),
        array("name"  =>"GS","code" => 500),
        array("name"  =>"GE","code" => 99534),
        array("name"  =>"SS","code" => 211),
        array("name"  =>"ES","code" => 34),
        array("name"  =>"LK","code" => 94),
        array("name"  =>"SD","code" => 249),
        array("name"  =>"SR","code" => 597),
        array("name"  =>"SJ","code" => 4779),
        array("name"  =>"SE","code" => 46),
        array("name"  =>"CH","code" => 41),
        array("name"  =>"SY","code" => 963),
        array("name"  =>"TW","code" => 886),
        array("name"  =>"TJ","code" => 992),
        array("name"  =>"TZ","code" => 255),
        array("name"  =>"","code" => 888),
        array("name"  =>"TH","code" => 66),
        array("name"  =>"","code" => 88216),
        array("name"  =>"TL","code" => 670),
        array("name"  =>"TG","code" => 228),
        array("name"  =>"TK","code" => 690),
        array("name"  =>"TO","code" => 676),
        array("name"  =>"MD","code" => 3732),
        array("name"  =>"MD","code" => 3735),
        array("name"  =>"TT","code" => 1868),
        array("name"  =>"SH","code" => 2908),
        array("name"  =>"TN","code" => 216),
        array("name"  =>"TR","code" => 90),
        array("name"  =>"TM","code" => 993),
        array("name"  =>"TC","code" => 1649),
        array("name"  =>"TV","code" => 688),
        array("name"  =>"UG","code" => 256),
        array("name"  =>"UA","code" => 380),
        array("name"  =>"AE","code" => 971),
        array("name"  =>"GB","code" => 44),
        array("name"  =>"US","code" => 1),
        array("name"  =>"","code" => 878),
        array("name"  =>"UY","code" => 598),
        array("name"  =>"VI","code" => 1340),
        array("name"  =>"UZ","code" => 998),
        array("name"  =>"VU","code" => 678),
        array("name"  =>"VA","code" => 3906698),
        array("name"  =>"VA","code" => 379),
        array("name"  =>"VE","code" => 58),
        array("name"  =>"VN","code" => 84),
        array("name"  =>"US","code" => 1808),
        array("name"  =>"WF","code" => 681),
        array("name"  =>"WF","code" => 967),
        array("name"  =>"YE","code" => 260),
        array("name"  =>"ZM","code" => 25524),
        array("name"  =>"ZW","code" => 263)
    );


    function getPrefix($number){
        $possiblePrefix = substr($number,0,7);
        $prefixList = array_column($this->countryData,"code");
        $index = -1;


        if(in_array($possiblePrefix, $prefixList)){
            $index = array_search($possiblePrefix, $prefixList);
        }

        if($index < 0){
            for($i=strlen($possiblePrefix); $i > 0; $i--){
                $possiblePrefix = substr($possiblePrefix, 0, $i-1);
                if(in_array($possiblePrefix, $prefixList)){
                    $index = array_search($possiblePrefix, $prefixList);
                    break;
                }

            }
        }

        if($index > -1){
            return $this->countryData[$index];
        }else{
            return false;
        }








    }

}