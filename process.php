<?php

require "PhoneParser.php";
require "Database.php";

$process = new PhoneParser();

$number =  $_POST['phone'];
$action = $_POST['action'];

$num = validateNumber($number);
$data = $process->parseNumber($num);

$dns = "mysql:host=nikah-app.c7r5b8wpdiej.us-east-1.rds.amazonaws.com;port=3306;dbname=assignment";
$username = "admin";
$password = "Asim1986";
$db = new Database($dns, $username, $password);

if($action == "add") {
    if ($data !== false) {
        $row = $db->getRow("all_phone_book", "*", array("prefix" => $data['prefix'], "number" => $data['code'] . $data['number']));
        if ($row) {
            //Get Last inserted row ID
            $id = $row['id'];
            //Check if deleted before
            if ($row['deleted'] == 1) {
                //Undelete row
                $db->updateRow("all_phone_book", array("deleted" => 0), array("id" => $row['id']));
                $message = "Deleted row re-activated with id: " . $row['id'];
            } else {
                $message = "Row already exist with id: " . $row['id'];
            }
        } else {
            //Insert Row
            $id = $db->insert("all_phone_book", array("prefix" => $data['prefix'], "number" => $data['code'] . $data['number']));
            $message = "New Row added. New row ID " . $id;
        }
        $response = array("result" => true, "id" => $id, "message" => $message);
    }
}

if($action == "search") {
    if ($data !== false) {
        $row = $db->getRow("all_phone_book", "*", array("prefix" => $data['prefix'], "number" => $data['code'] . $data['number']));
        if ($row) {
            $message = "Row found with ID: " . $row['id'];
            $response = array("result" => true, "id" => $row['id'], "message" => $message);
        } else {
            $message = "No row found.";
            $response = array("result" => false, "id" => 0, "message" => $message);
        }
    }
}

if($action == "delete"){
    $row = $db->getRow("all_phone_book","*", array("id" => $_POST['id']));
    if($row){
        $db->updateRow("all_phone_book",array("deleted" => 1), array("id" => $row['id']));
        $message = "Row deleted successfully";
        $response  = array("result" => true , "id" => $row['id'], "message" => $message);
    }else{
        $message = "No row found.";
        $response  = array("result" => false , "id" => 0, "message" => $message);
    }
}

echo json_encode($response);



function validateNumber($intNumber){
    //check if number contain international dialing code
    if(!substr($intNumber,0,1) == "+"){
        //Replace 00 with + sign
        if(substr($intNumber,0,2) == "00"){
            $intNumber = "+" . substr($intNumber, 2);
        }else{
            //Check if it starts with only one 0
            if(substr($intNumber,0,1) == "0"){
                $leading_digits = substr($intNumber,0,2);
                switch ($leading_digits){
                    case "07":
                    case "02":
                    case "03":
                        $intNumber = "+40" . substr($intNumber, 1);
                        break;
                    default:
                        $intNumber = "+49" . substr($intNumber, 1);
                        break;
                }
            }
        }
    }
    $num = str_replace(["-", "+"," "], "", $intNumber);
    return $num;
}