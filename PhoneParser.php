<?php

include "countryCodes.php";

class PhoneParser
{

    public function parseNumber($number){

        $countryCode = new countryCodes();
        $prefix = $countryCode->getPrefix($number);
        if($prefix === false){
            return false;
        }
        $number = implode("", explode($prefix['code'], $number, 2));
        return ["code" =>  "+" . $prefix['code'],  "prefix" => $prefix['name'], "number" => $number];

    }




}