<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <title>Test Script!</title>
</head>
<body>


<div class=" h-100 d-flex justify-content-center align-items-center" style="padding-top: 100px;">
    <div>
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Add New </button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Search</button>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent" style=" padding: 50px; width: 500px;">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                <form class="row g-3" id="formAdd">
                    <div class="col-auto">
                        <label for="inputPassword2" class="visually-hidden">Phone Number:</label>
                        <input type="text" class="form-control" id="inputPhone" name="phone" placeholder="Phone Number">
                    </div>
                    <div class="col-auto">
                        <input type="hidden" value="add" name="action"/>
                        <button type="submit" class="btn btn-primary mb-3">Add</button>
                    </div>
                </form>

                <div id="addAlert" class="alert" role="alert" style="display: none">
                </div>

            </div>
            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">


                <form class="row g-3" id="formSearch">
                    <div class="col-auto">
                        <label for="inputPassword2" class="visually-hidden">Phone Number:</label>
                        <input type="text" class="form-control" id="inputPhone" name="phone" placeholder="Phone Number">
                    </div>
                    <div class="col-auto">
                        <input type="hidden" value="search" name="action" />
                        <button type="submit" class="btn btn-primary mb-3">Search</button>
                    </div>
                </form>

                <div id="searchAlert" class="alert" role="alert" style="display: none;">
                </div>


            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>

<script>

    $(document).ready(function (e){

        $("#formAdd").submit(function(){
            if($("#formAdd #inputPhone").val().trim() == ""){
                alert("Please Enter phone number");
                return  false;
            }
            $.ajax({
                type: "POST",
                url: "/process.php",
                data: $(this).serialize(),
                success: function(data){
                    console.log(data);
                    if(data.result){
                        $("#addAlert").removeClass("alert-danger").addClass("alert-success");
                    }else{
                        $("#addAlert").removeClass("alert-success").addClass("alert-danger");
                    }
                    if(data.hasOwnProperty("message")){
                        $("#addAlert").html(data.message);
                        $("#addAlert").css("display","block");
                    }

                },
                dataType: "json"
            });
            return false;
        });

        $("#formSearch").submit(function(){
            if($("#formSearch #inputPhone").val().trim() == ""){
                alert("Please Enter phone number");
                return  false;
            }
            $.ajax({
                type: "POST",
                url: "/process.php",
                data: $(this).serialize(),
                success: function(data){
                    console.log(data);
                    if(data.result){
                        $("#searchAlert").removeClass("alert-danger").addClass("alert-success");
                    }else{
                        $("#searchAlert").removeClass("alert-success").addClass("alert-danger");
                    }
                    if(data.hasOwnProperty("message")){
                        let message = data.message + "<br />Do you want to <a class='btn btn-danger' href=javascript:deleteRow(" +  data.id + ')> Delete </a> this number?';
                        $("#searchAlert").html(message);
                        $("#searchAlert").css("display","block");
                    }

                },
                dataType: "json"
            });
            return false;
        });


    });

    function deleteRow(id) {
        console.log("ID: " + id);
        let params = {id: id, action: "delete", phone: $("#formSearch #inputPhone").val()};
        console.log(params);
        $.ajax({
            type: "POST",
            url: "/process.php",
            data: params,
            success: function(data){
                console.log(data);
                if(data.result){
                    $("#searchAlert").removeClass("alert-danger").addClass("alert-success");
                }else{
                    $("#searchAlert").removeClass("alert-success").addClass("alert-danger");
                }
                if(data.hasOwnProperty("message")){
                    let message = data.message;
                    $("#searchAlert").html(message);
                    $("#searchAlert").css("display","block");
                }

            },
            dataType: "json"
        });
        return false;
    }

</script>
</body>
</html>