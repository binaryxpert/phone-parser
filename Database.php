<?php

class Database extends PDO
{
    public function __construct($dsn, $username = null, $password = null, $options = null)
    {
        parent::__construct($dsn, $username, $password, $options);
    }


    public function getRow($table, $fileds = "*", $where = array()){
        $query = "SELECT $fileds FROM $table ";
        $params = [];
        if($where){
            $query .= " WHERE ";
            foreach ($where as $k => $v){
                $params[":" . $k] = $v;
                $q[] = " $k = :$k ";
            }
            $query .= implode(" and ", $q);
        }
        $sth = $this->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute($params);
        return $sth->fetch(PDO::FETCH_ASSOC);

    }

    public function updateRow($table, $fileds = "*", $where = array()){
        $query = "UPDATE $table ";
        $params = [];
        if($fileds){
            $query .= " SET ";
            foreach ($fileds as $k => $v){
                $params[":" . $k] = $v;
                $q[] = " $k = :$k ";
            }
            $query .= implode(" , ", $q);
        }
        if($where){
            $q = [];
            $query .= " WHERE ";
            foreach ($where as $k => $v){
                $params[":" . $k] = $v;
                $q[] = " $k = :$k ";
            }
            $query .= implode(" and ", $q);
        }
        $sth = $this->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute($params);

    }

    public function insert($table, $data){
        $query = "INSERT INTO $table SET ";
        $params = [];
        foreach ($data as $k => $v){
            $params[":" . $k] = $v;
            $q[] = " $k = :$k ";
        }
        $query .= implode(" , ", $q);
        $sth = $this->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute($params);
        return $this->lastInsertId();
    }

    public function delete($table, $where){
        $query = "DELETE FROM $table ";
        $params = [];
        if($where){
            $q = [];
            $query .= " WHERE ";
            foreach ($where as $k => $v){
                $params[":" . $k] = $v;
                $q[] = " $k = :$k ";
            }
            $query .= implode(" and ", $q);
        }
        $query .= implode(" , ", $q);
        $sth = $this->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute($params);
    }

}